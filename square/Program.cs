﻿using System;

namespace square
{
    class Program
    {
        //string[,] squareList = new string[4,4];

        //string tempString = "";
        public static string lengthX;
        public static string lengthY;
        public static string lolString = "HELLO";
        static void Main(string[] args)
        {
            StartMethod();
        }
        public static void StartMethod()
        {
            Console.WriteLine("Do you want a square or a rectangle?");
            string response = Console.ReadLine();
            if (response.ToLower() == "square")
            {
                PrintSquare();
            } else if (response.ToLower() == "rectangle")
            {
                PrintRectangle();
            } else
            {
                Console.WriteLine("Unrecognised input, try again");
                StartMethod();
            }
        }

        public static void PrintSquare()
        {
            Console.WriteLine("Enter dimensions of square: ");
            lengthY = Console.ReadLine();
            int Y = Convert.ToInt32(lengthY);

            for (int i = 0; i < Y; i++)
            {
                string tempString = " ";
                for (int z = 0; z < Y; z++)
                {
                    if (i == 0 || i == Y - 1)
                    {
                        tempString = tempString + "# ";
                    }
                    else if (z == 0 || z == Y - 1)
                    {
                        tempString = tempString + "# ";
                    }
                    else
                    { 
                        tempString = tempString + "  ";
                    }

                }
                Console.WriteLine(tempString);
            }

            EndMethod();
        }

        public static void PrintRectangle()
        {
            Console.WriteLine("Enter heigth of rectangle: ");
            lengthY = Console.ReadLine();
            int Y = Convert.ToInt32(lengthY);
            Console.WriteLine("Enter width of rectangle: ");
            lengthX = Console.ReadLine();
            int X = Convert.ToInt32(lengthX);

            for (int i = 0; i < Y; i++)
            {
                string tempString = " ";
                for (int z = 0; z < X; z++)
                {
                    if (i == 0 || i == Y - 1)
                    {
                        tempString = tempString + "# ";
                    }
                    else if (z == 0 || z == X - 1)
                    {
                        tempString = tempString + "# ";
                    }
                    else
                    {
                        tempString = tempString + "  ";
                    }

                }
                Console.WriteLine(tempString);
            }
            
            EndMethod();
        }

        public static void EndMethod()
        {
            Console.WriteLine("Enter Y to go again, or X to exit");
            string response = Console.ReadLine();
            if (response.ToLower() == "y" ) {
                StartMethod();
            } else if (response.ToLower() == "x")
            {
                Console.WriteLine("Goodbye!");
            } else
            {
                Console.WriteLine("Unrecognised response, try again: ");
                EndMethod();
            }
        }
    }
}
